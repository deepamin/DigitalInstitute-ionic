angular.module('starter')

.controller('PAdmission', function($scope, $state,$http ,$ionicPopup,$ionicLoading,APIUrl) {
  //var api_url="http://school.jmsofttech.com/api/";
  $scope.monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]; 
  
  $http.get(APIUrl+'YearRange')
    .success(function(data){
          if(JSON.stringify(data)!="[]"){
            $scope.isAdmissionYear=true;
            $scope.month_list=data;
          }else{
            alert(data);
            $scope.isAdmissionYear=false;
          }
    }).error(function(err){
      $ionicLoading.hide();
      alert("error");
    })  

    function getNumberDays(mon,year,day){
        var diff= new Date(year,mon,day)-new Date('1990-01-01 00:00:00');
        var days = ((((diff / 1000) / 60) / 60) / 24);
        return days;
    }


  $scope.monthId=function(month){
    var value=month.Value;
    month_arr=value.split('-');
    var month_name=month_arr[0];
    var year_name=month_arr[1];
    var monthNo = $scope.monthNames.indexOf(month_name)
    var start_day = getNumberDays(monthNo,year_name,1);
    var numOfDays = new Date(year_name, monthNo+1, 0).getDate();
    var last_day=start_day+numOfDays;
    //alert(last_day);
        // alert(getNumberDays($scope.monthNames.indexOf(month_name)+1,year_name,0));
    $http.get(APIUrl+'StudentAdmission/'+start_day+'/'+last_day)
    .success(function(data){
     // alert(data);
      if(JSON.stringify(data)!="[]"){
        $scope.isAdmission=true;
        $scope.admission_list=data;
      }else{
        $ionicPopup.alert({
            template:"Admission data is not available for this month",         
          });
        $scope.isAdmission=false;
      }
    }).error(function(err){
      $ionicLoading.hide();
      alert("error");
    })  
  }

})