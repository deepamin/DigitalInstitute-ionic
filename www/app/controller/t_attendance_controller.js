angular.module('starter')

.controller('TAttendanceCtrl', function($scope, $state, $ionicPopup, $location, ionicDatePicker, PostCallService, DateTimeService) {

    $scope.standard = ""
    $scope.std_div = ""
    $scope.isBatchSelected = false;
    $scope.isDateSelected = false;
    $scope.isLecSelected = false;
    $scope.currentDate = "Select Date";
    var monthNames = ["Jan", "Feb", "March", "April", "May", "June",
            "July", "Aug", "Sept", "Oct", "Nov", "Dec"
        ];
    // variables to store batch and lecture details
    var selectedBatch = null;
    var selectedLecture = null;

    // call service method to get menu_list and pass it to html page
    PostCallService.get('batch').then(function(batches){
        $scope.batches = batches;
    },function(msg){
        console.log("ctrl","msg");
        alert(msg);
    });

    // display date field on batch selection
    $scope.selectBatch = function(batch){
        selectedBatch = batch;
        $scope.isBatchSelected = true;
    }

    $scope.viewLectures = function(date){
        var days = DateTimeService.getNumOfDays(date);
        var url = `ViewMyLectures/${selectedBatch.Id}/${days}`;
        PostCallService.get(url).then(function(lectureDetail){
            if(lectureDetail.length == 0){
                return $ionicPopup.alert({
                    template:"No Lecture record available.",                   
                });
            }
            $scope.lectureDetail = lectureDetail;
            $scope.isDateSelected = true;
        },function(msg){
            console.log("ctrl","msg");
            alert(msg);
        });     
    }

    $scope.getLecDetail = function(lecture){
        var url = `GetStudentsForAttendance/${selectedBatch.Id}/${lecture.Id}`;
        selectedLecture = lecture;
        PostCallService.get(url).then(function(studList){
            if(studList.length == 0){
                return $ionicPopup.alert({
                    template:"No record available.",                   
                });
            }
            $scope.studList = studList
            $scope.isLecSelected = true;
        },function(msg){
            console.log("ctrl","msg");
            alert(msg);
        });     
    }




    $scope.updatePresentStatus = function(studentId,presentStatus){
        for (var i = 0; i < $scope.studList.length; i++) {
            if($scope.studList[i]['StudentId'] == studentId && presentStatus == true){
                $scope.studList[i]['IsPresent'] = 0;
            }else if($scope.studList[i]['StudentId'] == studentId && presentStatus == false){
                $scope.studList[i]['IsPresent'] = 1;
            } 
        };
    }

    $scope.send_data = function(){
        
        PostCallService.post("Attendance", $scope.studList).then(function(response){
            console.log(response)
        },function(msg){
            console.log("ctrl","msg");
            alert(msg);
        }); 
    }

    var ipObj1 = {
        callback: function (val) {  //Mandatory
            var selectedDate = new Date(val);
            $scope.viewLectures(new Date(val));
            $scope.currentDate = DateTimeService.getDateString(selectedDate);
        },
        inputDate: new Date(),      //Optional
        mondayFirst: true,          //Optional
        closeOnSelect: false,       //Optional
        templateType: 'popup'       //Optional
    };

    $scope.openDatePicker = function(){
        ionicDatePicker.openDatePicker(ipObj1);
    };
})
 
