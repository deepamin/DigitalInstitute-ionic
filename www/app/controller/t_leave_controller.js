angular.module('starter')

.controller('TApplyLeave', function($scope, $state,$http ,$ionicPopup,$ionicLoading,LeaveApplyService,PostCallService,APIUrl) {
	//var api_url="http://school.jmsofttech.com/api/";

		$scope.applyLeave = function(leave){
			var _postUrl="TeacherLeave";
			if(leave.ReasonOfLeave==""){
				$ionicPopup.alert({
					template:"Please Enter Reason For Leave",					
				});	
			}else if(leave.NoOfDays==""){
				$ionicPopup.alert({
					template:"Please Enter No of days for Leave",					
				});	
			}else{					
				PostCallService.post(_postUrl, leave).then(function(leaveData){
					if (leaveData.length==0) {
							$ionicPopup.alert({
								template:"There is some error,Your leave request have not sent",					
							});
					}else{
							clear(leave);		
							$ionicPopup.alert({
								template:"Leave Succefully Applied",					
							});		
					}
						// alert(JSON.stringify(leaveData));
					},function(msg){
						alert(msg);
					});
			}	
		
		}

		var clear=function(leave){
			leave.NoOfDays='';	
			leave.ReasonOfLeave='';
		}


})