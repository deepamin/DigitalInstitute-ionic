angular.module('starter')

.controller('SComplainDetailCtrl', function($scope, $state,$http ,$ionicLoading,$ionicPopup,$stateParams,complainService,PostComplainService,PostCallService,APIUrl) {
	
	var getComplainData= function(){
		PostCallService.get('StudentBehaviourNoticeDetail/'+complainService.getId()).then(function(complain){
			if(complain=="[]"){
				detail_available=true;				
			}else{
				$scope.complain_detail_data=complain;
				detail_available=false;
			}
			},function(error){
				alert(error);		
		});	
	}
	
	getComplainData();
	
	$scope.sendComplains=function(postComplain){
	
			var complain='{"StudentBehaviourNoticeId":'+complainService.getId()+', "Comment":"'+postComplain.message+'"}';
			var postComplainUrl="SaveStudentBehaviourComment";
			PostCallService.post(postComplainUrl,complain).then(function(status){
				$ionicPopup.alert({
					title:'Success',
					template:status,					
				});		
				getComplainData();	
			});	

			postComplain.message="";
		}
	
})
