angular.module('starter')

.controller('SComplainCtrl', function($scope, $state,$http ,$ionicPopup,$ionicLoading,complainService,PostCallService,APIUrl) {
	//alert(APIUrl);
	this._url=APIUrl;
	PostCallService.get('StudentBehaviourNotice').then(function(data){
		if(JSON.stringify(data)=="[]"){
			complain_available=true;	
			$ionicPopup.alert({
				template:"Complain data is not available",					
			});			
		}else{
			$ionicLoading.hide();
			$scope.complain_data=data;
			complain_available=false;
		}
	},function(msg){
		alert("error");
	});

	
	$scope.getComplainDetails=function(complain){
		complainService.setId(complain.Id);
		$state.go('complaindetails');
		
	}
})
