angular.module('starter')

.controller('TBehaviourNotice', function($scope, $state,$http ,$ionicPopup, $ionicLoading, PostCallService, APIUrl) {
	// var api_url="http://school.jmsofttech.com/api/";
		
		$ionicLoading.show();
		PostCallService.get("course").then(function(course){
			$ionicLoading.hide();
			if(!course.length){
				$scope.hasCourse=true;					
				$ionicPopup.alert({
					template:"No Course Data Available",					
				});		
			}else{
				$scope.hasCourse=false;	
				$scope.course_list=course;
			}
		},function(error){
				$ionicLoading.hide();
				alert("error");		
		});
	
		$scope.getCourseId=function(course){
			id=course.Id;	
			$ionicLoading.show();	
			PostCallService.get('Batch/'+id).then(function(batch_data){
				$ionicLoading.hide();
				if(!batch_data.length){
					$scope.hasBatches=true;					
					$ionicPopup.alert({
						template:"No Batch Data Available",					
					});		
				}else{
					$scope.hasBatches=false;	
					$scope.batch_list=batch_data;
				}
				},function(error){
					$ionicLoading.hide();
					alert("error");	
			});
		}

		$scope.getStudentId=function(student){
		//	$scope.student_obj=student;
			alert(JSON.stringify(student));
			$scope.stud_data = { student_name : student.Name};

			 var myPopup = $ionicPopup.show({
      					templateUrl: 'app/layout/t_dialog_behaviour_notice.html',
      					title: "Notice",
      					scope: $scope,	
      					 buttons: [{
					         text: 'Cancel',
							}, {
							 text: '<b>Save</b>',
							 type: 'button-positive',
							 onTap:function(){
							 	alert($scope.stud_data.comment);
							 	if ($scope.stud_data.comment=="") {
							 		$ionicPopup.alert({
										template:"Please Enter Comment",					
									});		
							 	}else{
							 		var _jsonNotice='{"StandardId":'+_std+', "ClassTypeId":'+_cls+', "StudentId":'+student.Id+',"Comment":"'+$scope.stud_data.comment+'"}'
									saveComplain(_jsonNotice);				
							 	};
							 		
							 }	
							}]
				});
		}
		var saveComplain=function(complain){
			var _postUrl="StudentBehaviourNotice";	
			PostCallService.post(_postUrl,complain).then(function(status){
				$ionicPopup.alert({
					title:"success",
					template:"Your Comment sent successfully",					
				});
			})	
		}

})