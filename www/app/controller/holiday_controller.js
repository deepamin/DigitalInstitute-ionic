angular.module('starter')

.controller('HolidayCtrl', function($scope, $state, $ionicPopup, $window, $location, PostCallService) {
	
	// call service method to get menu_list and pass it to html page
	PostCallService.get("Holiday").then(function(holidays){
		if(holidays.length==0){
			$ionicPopup.alert({
				template:"No data is available for Holidays",					
			});
		}else{
			$scope.holiday_list = holidays;
		}
	},function(error){
		console.log("ctrl","msg");
	});

	$scope.dateFormatter = function(date){
		// console.log(date.split(' ')[0]);
		date=date.split(' ')[0];
		return date;
	}

})
 
