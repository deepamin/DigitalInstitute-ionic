angular.module('starter')

.controller('AppCtrl', function($scope, $state, $ionicPopup, $location, PostCallService) {
	var loginUrl="Authenticate/AuthorizationToken";
	var BranchPopup;
	$scope.login = function(data) {

	//		$location.path('/dashboard')
		console.log(JSON.stringify(data))
		PostCallService.post(loginUrl,data).then(function(loginData){
			if (loginData){
				window.localStorage.setItem('Token', loginData['Token']);
				window.localStorage.setItem('UserType', loginData['UserType']);
				if (loginData['Branches']!=null) {
							
					showBranchList(loginData['Branches']);
					// alert(loginData['Branches'][1]['Name']);	
				}else{
					$location.path('/dashboard')	
					// alert("student");	
				};	

				
			}
			
		},function(msg){
			console.log("ctrl","msg");
			alert("Error");
		});
	};

	var showBranchList=function(Branch){
		$scope.branch_list=Branch;	

		BranchPopup = $ionicPopup.show({
      					templateUrl: 'app/layout/dialog_select_branch.html',
      					title: "Notice",
      					scope: $scope,	
      				});

	};	

	$scope.getBranchId =function(branch){
		window.localStorage.setItem('BranchId', branch.Id);
		$location.path('/dashboard')
		BranchPopup.close();
		
	}  
})
 
