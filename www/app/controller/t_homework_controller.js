angular.module('starter')

.controller('THomeWork', function($scope, $http, $ionicPopup, $ionicLoading, DateTimeService, ionicDatePicker, APIUrl, PostCallService) {
	//var api_url="http://school.jmsofttech.com/api/";
	var days;
	var _lectureId;
	$scope.hasBatch = false;
	$scope.currentDate = 'Select Date';
	
	//global variables
	var selectedDate = null;
	var selectedBatch = null;
	var selectedLecture = null;

	// call service method to get menu_list and pass it to html page
    PostCallService.get('batch').then(function(batches){
    	console.log(batches);
        $scope.batchList = batches;
    },function(msg){
        console.log("ctrl","msg");
        alert(msg);
    });


	$scope.getLectureDetails=function(batch){
		// If date is not given, by default today's date is taken.
		if(!selectedDate){
			date = new Date();
		}
        selectedBatch = batch;
		date = new Date(selectedDate.getFullYear(),selectedDate.getMonth(),selectedDate.getDate());
		// Assuming date is a string of parsable format: ie. "2014-01-27T01:00:00+00:00"
		var diff = date - new Date('1990-01-01 00:00:00');
		// Calculate from milliseconds
		days = ((((diff / 1000) / 60) / 60) / 24);
	// 	// alert(days);
	// 	PostCallService.get('Batch').then(function(data){
	// 		alert(JSON.stringify(data));	
	// 		$ionicLoading.hide();
			
	// 		if(JSON.stringify(data)!="[]"){
	// 			$scope.hasBatch=true;
	// 			$scope.batch_list=data;
	// 		}else{
	// 			$ionicPopup.alert({
	// 				template:"Batch Data is not available",					
	// 			});
	// 			$scope.hasBatch=false;
	// 		}
	// 	},function(msg){
	// 		$ionicLoading.hide();
	// 		console.log("ctrl","msg");
	// 		alert(msg);
	// 	});
		
	// 	// $ionicLoading.show();
	// }
	// $scope.getBatchId=function(batch){
	// 	// alert(batch.Id);
	// 	PostCallService.get('ViewMyLectures/'+batch.Id+'/'+days).then(function(data){
		$ionicLoading.show();
		var url = `ViewMyLectures/${selectedBatch.Id}/${days}` 
		PostCallService.get(url).then(function(data){
			$ionicLoading.hide();
			
			if(data.length){
				$scope.hasLecture=true;
				$scope.lectureList=data;
			}else{
				$ionicPopup.alert({
					template:"Lecture Data is not available",					
				});
				$scope.hasLecture=false;
			}
		},function(msg){
			$ionicLoading.hide();
			console.log("ctrl","msg");
			alert(msg);
		});
	
	}



	$scope.getHomeworkDetails=function(lecture){
		selectedLecture = lecture;
		$ionicLoading.show();
		var url = `ViewHomeWork/${days}/${lecture.Id}`
		PostCallService.get('ViewHomeWork/'+days+'/'+lecture.Id).then(function(data){
			$ionicLoading.hide();
			if(JSON.stringify(data)!="null"){
	/*			alert("true");*/
				$scope.hasHomework=true;
				$scope.lesson=data;
			}else{
			/*	alert("false");
				$ionicPopup.alert({
					template:"No Homework Detail Available",					
				});*/				
				$scope.hasHomework=true;
			}
		},function(msg){
			$ionicLoading.hide();
			console.log("ctrl","msg");
			alert(msg);
		});
	}

	$scope.saveHomework=function(lesson){
		//alert(lesson.HomeWork);
		if (lesson.HomeWork=="") {
			$ionicPopup.alert({
				template:"Please Enter HomeWork!",					
			});
		}else if (lesson.ClassWork=="") {
			$ionicPopup.alert({
				template:"Please Enter ClassWork!",					
			});	
		}else{
			var _JsonData='{"Date":'+days+' , "LectureId":'+selectedLecture.Id+',"HomeWork":"'+lesson.HomeWork+'","ClassWork":"'+lesson.ClassWork+'"}'
			PostCallService.post("HomeWork", _JsonData).then(function(success){
				$ionicPopup.alert({
					template:success,					
				});	
			})
		}
	};

	function getCurrentDate(date){
		var year = date.getFullYear();
        var month = date.getMonth();
        var day = date.getDate();
        return year + "-" + month + "-" + day;
	}

	var ipObj1 = {
      	callback: function (val) {  //Mandatory
        	selectedDate = new Date(val);
        	$scope.currentDate = DateTimeService.getDateString(selectedDate);;
        	if($scope.batchList.length)
        		$scope.hasBatch = true;
        	else
        		$ionicPopup.alert({
					template:"No data available for given date.",					
				});
      	},
		inputDate: new Date(),      //Optional
		mondayFirst: true,          //Optional
		closeOnSelect: false,       //Optional
		templateType: 'popup'       //Optional
    };

    $scope.openDatePicker = function(){
    	// alert("CLick");
      	ionicDatePicker.openDatePicker(ipObj1);
    };
		
})