angular.module('starter')

.controller('ExamTimeTableCtrl', function($scope, $state, $ionicLoading,$ionicPopup, $window, $location, ionicDatePicker,PostCallService) {
	$scope.schedule_list = [];
	$scope.startDays=0;
	$scope.endDays=0;
	// call service method to get menu_list and pass it to html page	
	PostCallService.get("StandardExamMaster").then(function(dashboard_data){
		$ionicLoading.hide();
		if (dashboard_data=="[]") {
			$ionicPopup.alert({
				template:"Exam is Not Available",					
			});					
		}else{
			$scope.timtble_list = dashboard_data;			
		}
	},function(msg){
		$ionicLoading.hide();
		alert(msg);	
	});

	// method to get data of selected option
	$scope.display_info = function(){
		$ionicLoading.show();
		PostCallService.get("ExamSchedule/"+$scope.startDays+"/"+$scope.endDays).then(function(schedule_data){
			$ionicLoading.hide();
			if (schedule_data.length==0) {
				$ionicPopup.alert({
					template:"Exam Schedule is Not Available",					
				});					
			}else{
				$scope.schedule_list= schedule_data;			
			}	
		});
	}

	$scope.dateFormatter = function(date){
		date=date.split(' ')[0];
		return date;
	}

	function getCurrentDate(date){
		var year = date.getFullYear();
        var month = date.getMonth();
        var day = date.getDate();
        return year + "-" + month + "-" + day;
	}

	function dateDiffernce(date=''){
		if(date==''){
			date = new Date();
		}
		date = new Date(date.getFullYear(),date.getMonth(),date.getDate());
		// Assuming date is a string of parsable format: ie. "2014-01-27T01:00:00+00:00"
		var diff = date - new Date('1990-01-01 00:00:00');
		// Calculate from milliseconds
		days = ((((diff / 1000) / 60) / 60) / 24);
		return days;	
	}


	/* start Date Picker */


	var startObj = {
	
      	callback: function (val) {  //Mandatory
        	var selectedDate = new Date(val);
        		// alert($scope.endDays);
      		$scope.startDays=dateDiffernce(selectedDate); 	
 	   	    $scope.startDate = getCurrentDate(selectedDate);  	

        	if($scope.startDays < $scope.endDays ){
				$scope.display_info();
				// alert("ok");        	
        	}else{
        			if ($scope.endDays !=0) {
    	    			$ionicPopup.alert({
							template:"Please select correct Start date",					
						});
    	    		};	
        		// alert("Not Ok");
        	}
      	},

		inputDate: new Date(),      //Optional
		mondayFirst: true,          //Optional
		closeOnSelect: false,       //Optional
		templateType: 'popup'       //Optional
    };

	$scope.openStartDatePicker=function(){		      
		ionicDatePicker.openDatePicker(startObj);
	}


	/* end date picker */


	var endObj = {
      	callback: function (val) {  //Mandatory
        	var selectedDate = new Date(val);
        	$scope.endDays=dateDiffernce(selectedDate); 	
        	$scope.endDate = getCurrentDate(selectedDate);	
      		
      		if($scope.startDays < $scope.endDays ){
				$scope.display_info();
				// alert("ok");        	
        	}else{
        			if ($scope.startDaysDays !=0) {
    	    			$ionicPopup.alert({
							template:"Please select correct end date",					
						});
    	    		};	
        		// alert("Not Ok");
        	}
      	},
		inputDate: new Date(),      //Optional
		mondayFirst: true,          //Optional
		closeOnSelect: false,       //Optional
		templateType: 'popup'       //Optional
    };

	$scope.openEndDatePicker=function(){
		ionicDatePicker.openDatePicker(endObj);
	}

})
 
