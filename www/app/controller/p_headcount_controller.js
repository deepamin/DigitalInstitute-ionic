angular.module('starter')
.controller('PHeadcount', function($scope, $state,$http ,$ionicPopup,$ionicLoading,APIUrl) {
//	var api_url="http://school.jmsofttech.com/api/";

	$scope.getUser=function(user){
		$ionicLoading.show();
		if(user=="employee"){
			$http.get(APIUrl+'EmployeeHeadCount')
			.success(function(data){
				$ionicLoading.hide();
				if(JSON.stringify(data)!="null"){
					$scope.isEmployee=true;
					$scope.isStandard=false;
					$scope.isSHeadCount=false;
					$scope.total=data.Total;
					$scope.male=data.TotalMale;
					$scope.female=data.TotalFemale;	
				}else{
					$ionicLoading.hide();
					$scope.isEmployee=false;
					$scope.isStandard=false;
					$scope.isSHeadCount=false;
					$ionicPopup.alert({
						template:"Data is not available",					
					});	
				}
			}).error(function(err){
				$ionicLoading.hide();
				alert("error");
			})	
		}else if(user=="student"){
				$http.get(APIUrl+'Standard')
				.success(function(data){
					$ionicLoading.hide();
					if(JSON.stringify(data)!="[]"){
						$scope.isEmployee=false;		
						$scope.isStandard=true;
						$scope.standard_list=data;
					}else{
						$ionicLoading.hide();			
						// alert(data);
						$ionicPopup.alert({
							template:"Data is not available",					
						});	
						$scope.isStandard=false;
					}
				}).error(function(err){
					$ionicLoading.hide();
					alert("error");
				})		
			}	
		}

	$scope.standardId=function(std){
		$ionicLoading.show();

		$http.get(APIUrl+'StudentHeadCount/'+std.Id)
			.success(function(data){
				if(JSON.stringify(data)!="[]"){
					$ionicLoading.hide();				
					$scope.isSHeadCount=true;
					$scope.isStandard=true;
					$scope.std_list=data;
				}else{
					$ionicLoading.hide();				
					// alert(data);
					$scope.isSHeadCount=false;
					$ionicPopup.alert({
						template:"Data is not available",					
					});
				}
			}).error(function(err){
				$ionicLoading.hide();
				alert("error");
			})

	}	

})