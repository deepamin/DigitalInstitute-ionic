(function (app) {

    var monthNames = ["Jan", "Feb", "March", "April", "May", "June",
            "July", "Aug", "Sept", "Oct", "Nov", "Dec"
        ];

    function Service($http, $q) {
        this._$http = $http;
        this._$q = $q;
    }
    
    // return number of days from 1st Jan 1990 till given date
    Service.prototype.getNumOfDays = function(date){
        // Assuming date is a string of parsable format: ie. "2014-01-27T01:00:00+00:00"
        var diff = new Date(date) - new Date('1990-01-01 00:00:00');
        // Calculate from milliseconds
        return ((((diff / 1000) / 60) / 60) / 24);
    }

    // return date string of given date 
    Service.prototype.getDateString = function(date){
        var year = date.getFullYear();
        var month = date.getMonth();
        var day = date.getDate();
        return day + " " + monthNames[month] + ", " + year;
    };

    Service.$inject = ['$http', '$q'];

    app.service("DateTimeService", Service);
})(appMain) 