(function (app) {

    function Service($http, $q,APIUrl) {
        this._$http = $http;
        this._$q = $q;
        this._url=APIUrl;
    }
    // method to get menu_list to display on dashboard based on given data.
    Service.prototype.get_holiday_list = function () {
        var deferred = this._$q.defer();
        var data = window.localStorage.getItem('Token')
        var url = this._url+"Holiday";
           this._$http({
                method: 'GET',
                // url:'holiday.txt'
                url: url,
           }).success(function (data, status, headers, cfg) {
                deferred.resolve(data);
            }).error(function (err, status) {
                alert(err)
                deferred.reject(status);
            });
            return deferred.promise;
    };

    Service.$inject = ['$http', '$q','APIUrl'];

    app.service("HolidayService", Service);
})(appMain) 