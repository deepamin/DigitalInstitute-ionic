// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var appMain=angular.module('starter', ['ionic', 'ngCordova', 'ionic-datepicker','chart.js']);
var siteUrl = "http://institute.jmsofttech.com/api/";
// var siteUrl = "http://school.jmsofttech.com/api/";
var paperUrl = "http://school.jmsofttech.com/";
appMain.run(function($ionicPlatform, $rootScope, $state, $stateParams) {
  $state.go('login');
  $rootScope.$state = $state;
  $rootScope.menuList = [];
  $rootScope.navTitle = 'Digital Pathshala';
  $rootScope.$stateParams = $stateParams;
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }

    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});

    appMain.value('APIUrl', siteUrl);
    appMain.value('PAPERUrl', paperUrl);

appMain.config(function ($stateProvider, $urlRouterProvider, $httpProvider ) {
  $stateProvider
  // to redirect to login module
  .state('login', {
    url: '/login',
    templateUrl: 'app/layout/login.html',
    controller: 'AppCtrl'
  })
  // to redirect to dashboard module
  .state('dashboard', {
    url: '/dashboard',
    templateUrl: 'app/layout/dashboard.html',
    controller: 'DashCtrl'
  })

  .state('attendance',{
    url:'/attendance',
    templateUrl: 'app/layout/attendance.html',
    controller: 'AttendanceCtrl'
  })  

  //teacher's module
  //to redirect to attendance module
  .state('t_attendance',{
    url:'/t_attendance',
    templateUrl: 'app/layout/t_attendance.html',
    controller: 'TAttendanceCtrl'
  })

  //to redirect to exammark module
  .state('t_exammark',{
    url:'/t_exammark',
    templateUrl: 'app/layout/t_exammark.html',
    controller: 'TExammarkCtrl'
  })

  // to redirect to result module
  .state('result', {
    url: '/stud_result',
    templateUrl: 'app/layout/stud_result.html',
    controller: 'SResultCtrl'
  })
  // to redirect to exam-timetable module
  .state('examtimetable', {
    url: '/examtimetable',
    templateUrl: 'app/layout/examtimetable.html',
    controller: 'ExamTimeTableCtrl'
  })
  //to redirect to ComplainBox Module
  .state('complainbox', {
    url: '/complainbox',
    templateUrl: 'app/layout/s_complain_box.html',
    controller: 'SComplainBox'
  })
  // to redirect to holiday module
  .state('holidaylist', {
    url: '/holidaylist',
    templateUrl: 'app/layout/holiday.html',
    controller: 'HolidayCtrl'
  })
  .state('viewcomplain', {
    url: '/stud_view_complain',
    templateUrl: 'app/layout/stud_complain.html',
    controller: 'SComplainCtrl'
  })
  .state('complaindetails', {
    url: '/complain_details',
    templateUrl: 'app/layout/stud_complain_details.html',
    controller: 'SComplainDetailCtrl'
  })
  .state('homework', {
    url: '/homework',
    templateUrl: 'app/layout/stud_homework.html',
    controller: 'SHomework'
  })
  .state('stodaytimetable', {
    url: '/stodaytimetable',
    templateUrl: 'app/layout/s_today_timetable.html',
    controller: 'STodayTimetable'
  })
  .state('ssamplepaper', {
    url: '/ssamplepaper',
    templateUrl: 'app/layout/s_upload_sample_paper.html',
    controller: 'SSamplePaper'
  })
  .state('timetable', {
    url: '/timetable',
    templateUrl: 'app/layout/p_timetable.html',
    controller: 'PTimetable'
  })
  .state('headcount', {
    url: '/headcount',
    templateUrl: 'app/layout/p_headcount.html',
    controller: 'PHeadcount'
  })
  .state('viewadmission', {
    url: '/viewadmission',
    templateUrl: 'app/layout/p_view_admission.html',
    controller: 'PAdmission'
  })
  .state('lbteacherlist', {
    url: '/teacherlist',
    templateUrl: 'app/layout/p_teacher_logbook/p_teacher_list.html',
    controller: 'PTLogBook'
  })
  .state('logbookdetail', {
    url: '/logBookdetail',
    templateUrl: 'app/layout/p_teacher_logbook/p_logbook_detail.html',
    controller: 'PTLogBook'
  })
  .state('lvteacherlist', {
    url: '/teacherleave',
    templateUrl: 'app/layout/p_teacher_leave/p_leave_teacher_list.html',
    controller: 'PTLeaveCtrl'
  })
  .state('t_behaviour_notice', {
    url: '/stduentbehaviournotice',
    templateUrl: 'app/layout/t_behaviour_notice.html',
    controller: 'TBehaviourNotice'
  })
  .state('t_logbook', {
    url: '/teacherlogbook',
    templateUrl: 'app/layout/t_logbook.html',
    controller: 'TLogBook'
  })
  .state('t_homework', {
    url: '/teacherhomework',
    templateUrl: 'app/layout/t_homework.html',
    controller: 'THomeWork'
  })
  .state('t_apply_leave', {
    url: '/teacherapplyleave',
    templateUrl: 'app/layout/t_apply_leave.html',
    controller: 'TApplyLeave'
  })
  .state('cmn_notification', {
    url: '/notification',
    templateUrl: 'app/layout/cmn_notification.html',
    controller: 'CmnNotification'
  })
  .state('t_today_schedule', {
    url: '/teacherschedule',
    templateUrl: 'app/layout/t_today_schedule.html',
    controller: 'TTodaySchedule'
  })
  .state('p_inquiry_graph', {
    url: '/inquiry',
    templateUrl: 'app/layout/p_graph_layout.html',
    controller: 'GraphCtrl'
  })
  .state('p_admission_graph', {
    url: '/admission',
    templateUrl: 'app/layout/p_graph_layout.html',
    controller: 'GraphCtrl'
  })
  // $urlRouterProvider.otherwise('/login');
  $urlRouterProvider.otherwise('/login');
  $httpProvider.defaults.headers.post['Content-Type'] = 'application/json';
  $httpProvider.interceptors.push('httpRequestInterceptor');
  $httpProvider.defaults.useXDomain = true;
  delete $httpProvider.defaults.headers.common['X-Requested-With'];
  // $httpProvider.defaults.headers.post['Content-Type'] = "application/json";
  
  // $httpProvider.defaults.useXDomain = true;
  
});

appMain.controller('MainCtrl', function($scope, $ionicScrollDelegate,$rootScope,$ionicHistory) {
  $scope.statePath = {
      1:'t_attendance',
      2:'t_homework',
      3:'t_behaviour_notice',
      4:'t_logbook',
      // 5:'t_apply_leave',
      5:'t_today_schedule',
      6:'t_exammark',
      8:'',
      9:'cmn_notification',
      10:'attendance',
      12:'examtimetable',
      13:'result',
      14:'viewcomplain',
      15:'homework',
      16:'cmn_notification',
      17:'complainbox',
      18:'timetable',
      // 19:'headcount',
      19:'',
      21:'viewadmission',
      22:'lvteacherlist',
      26:'lbteacherlist',
      24:'uploadedpaper',
      25:'holidaylist',
      23:'logout',
      27:'cmn_notification',
      29:'stodaytimetable'
  } 

  var titleNames = {
    'login':'Login',
    'dashboard':'Dashboard',
    'attendance':'Attendance',
    't_attendance':'Attendance',
    't_exammark':'Attendance',
    'result':'Result',
    'examtimetable':'Exam Timetable',
    'complainbox':'Complain',
    'holidaylist':'Holiday List',
    'viewcomplain':'View complain',
    'complaindetails':'Complain details',
    'homework':'Home work',
    'timetable':'Timetable',
    'headcount':'Head count',
    'viewadmission':'Admission',
    'lbteacherlist':'Teacher List',
    'logbookdetail':'Logbook detail',
    'lvteacherlist':'Teacher List',
    't_behaviour_notice':'Behaviour Notice',
    't_logbook':'Logbook',
    't_homework':'Homework',
    't_apply_leave':'Apply Leave',
    'cmn_notification':'Notification',
    't_today_schedule':'Schedule',
    'stodaytimetable':"Today's Timetable",
  }
  $scope.toggleLeft = function() {
      $ionicSideMenuDelegate.toggleLeft();
    };
  
  $scope.hideMenuBtn = false;
  $rootScope.$on('$stateChangeSuccess', function (event, next) { 
    $rootScope.navTitle = titleNames[$ionicHistory.currentStateName()];
    if($ionicHistory.currentStateName() == 'login'){
        $scope.hideMenuBtn = true;
    }else{
        $scope.hideMenuBtn = false;
    }
  });
});
